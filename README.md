# TreeDensityTool

_Part of the "EnvNeuro Python GIS Toolbox", a Python project for extracting multiple types of geodata from large datasets, for lists of coordinates from participants in EnvNeuro studies. This overarching project is currently in early theoretical stages, although individual tools like this one are already in a usable standalone state._

This is a tool that, for a given input of coordinates in Europe, calculates the Tree Density using the [Copernicus 2018 Tree Cover Density Dataset](https://land.copernicus.eu/en/products/high-resolution-layer-tree-cover-density/tree-cover-density-2018).

Please use the 10m version from 2018. This tool does not currently support any other versions.

Usage:
1. Install Python requirements
2. Drop the unzipped dataset somewhere into the root directory. (The exact structure doesn't matter, the code will scan subdirectories for the images automatically)
3. Make sure you have a task file inside the root directory. The support here is currently specifically tailored towards one dataset, others may or may not work. Your dataset must be in WGS84 or EPSG:25832. Make sure it is comma seperated, and that the topmost line has labels for the columns, ideally "id", "X" and "Y". Shapefiles are not currently supported but this is a future goal.
4. Run `python3 TreeDensityTool.py [filename] [radius]`. If you don't specify a radius, it'll do 100, 200, 500, 1000 and 2000 in succession.
5. When using larger radii like 2000m, bring some patience as the calculations take somewhere betweem 0.1s to 1s per data point, depending on the speed of your computer.
