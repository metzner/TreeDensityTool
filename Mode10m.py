import math
import os
import statistics
from functools import lru_cache
from typing import Union

import numpy as np
from PIL import Image
from numpy import typing as npt

Image.MAX_IMAGE_PIXELS = 100000000


@lru_cache(maxsize=12)
def get_image(x: int, y: int) -> npt.NDArray[Union[int, float]]:
    file_name = f"TCD_2018_010m_E{x}N{y}_03035_v020.tif"

    full_path = None

    for root, dirs, files in os.walk(os.path.dirname(os.path.realpath(__file__))):
        if file_name in files:
            full_path = os.path.join(root, file_name)

    if not full_path:
        print(f"Couldn't find {file_name}. Please download the Copernicus 2018 tree density dataset.")
        assert False

    #  print(f"Retrieving {full_path}.")

    assert os.path.isfile(full_path), f"Could not find file {file_name}"

    im = Image.open(full_path)

    return np.array(im, dtype=float)


def get_desired_pixels(x, y, radius_m=200):
    pixels = []

    for potential_x in range(round(x) - radius_m // 10 - 2, round(x) + radius_m // 10 + 2, 1):
        for potential_y in range(round(y) - radius_m // 10 - 2, round(y) + radius_m // 10 + 2, 1):
            distance = math.sqrt(((potential_x - x) * 10) ** 2 + ((potential_y - y) * 10) ** 2)

            if distance <= radius_m:
                pixels.append((potential_x, potential_y))

    return pixels


def get_single_tree_density_10(x: Union[int, float], y: Union[int, float], radius: Union[int, float], disabled_0: bool):
    # find the closest pixel center (10m apart, 5 at the end)
    closest_x = int(round(x + 5, -1) - 5)
    closest_y = int(round(y + 5, -1) - 5)

    # The top left pixel of an image represents e.g. x = 4000005, y = 2999995
    # Y decrease as you go down on the image, X increases as you go right
    # We need to be careful as the top left pixel of an image is (0,0).
    # This means the geographical Y goes down as the y pixel goes up.
    # So now, we find the next lowest x value like this: XX00005, and the next *highest* y value like this: 29XXXX5.
    closest_x_below = closest_x + 5 - closest_x % 100000
    closest_y_above = closest_y - 5 - closest_y % 100000 + 100000

    # Get the difference between the coordinates representing the top left of the image & the actual X/Y coordinates
    difference_x = abs(closest_x_below - x)
    difference_y = abs(closest_y_above - y)

    # Now that we know how many meters the coordinate is away from the coordinate representing the top left pixel,
    # We can figure out how many *pixels* it is away from the top left *pixel*.
    # For now, we'll keep the significant digits after the decimal point.
    pixel_x_from_left = difference_x / 10
    pixel_y_from_top = difference_y / 10

    # The files are split up. If our top left pixel is at XX00005, YY99995, then the file is called EXXNYY.
    x_file = closest_x_below // 100000
    y_file = closest_y_above // 100000

    # Get the desired pixels in a radius. These will now be pairs of integer values, representing pixels.
    desired_pixels = get_desired_pixels(pixel_x_from_left, pixel_y_from_top, radius)

    values = []

    # x, y are pixels in the file defined by x_file (E) and y_file (N)
    for x, y in desired_pixels:
        # It is possible that we have now "switched files" if either x or y are now below 0 or above 10000
        # If x < 0, we need to decrease the E number. If y < 0, we need to **increase** the N number.
        real_x_file = x_file + (x // 10000)
        real_y_file = y_file - (y // 10000)

        # After doing that, normalise x and y to the 0-10000 range again.
        real_x = x % 10000
        real_y = y % 10000

        # Add that pixel to the list of values.
        values.append(float(get_image(real_x_file, real_y_file)[real_y][real_x]))

    if not values:
        raise ValueError(f"Couldn't get any pixels for {x},{y}")

    len_before = len(values)

    unknown_values = [value for value in values if not (0 <= value <= 100) and not value == 255]

    if unknown_values:
        print(f"Encountered unknown pixel values: {set(unknown_values)}.")

    if disabled_0:
        values = [value if (1 <= value <= 100) else 0 for value in values]
        len_after = sum((0 <= value <= 100) for value in values)
    else:
        values = [value for value in values if (0 <= value <= 100)]
        len_after = len(values)

    if not len_after:
        print(f"None of the pixels for {x},{y} were valid.")
        return ".",".",len_after, len_before - len_after

    elif len_after == 1:
        print(f"Only one of of the pixels for {x},{y} was valid, which means stddev cannot be calculated.")
        return round(statistics.mean(values), 5), ".", len_after, len_before - len_after

    elif len_before != len_after:
        print(f"Some values were invalid for {x},{y}. Out of {len_before} pixels, {len_after} were valid.")

    return round(statistics.mean(values), 5), round(statistics.stdev(values), 5), len(values), len_before - len_after
