import importlib.util
import sys
import os.path
import logging
from dataclasses import dataclass, field
from functools import lru_cache
from typing import List, Optional

import reverse_geocode

from pyproj import Transformer

from BaseClasses import ToolDefinition, ToolDefinitionArgs

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

modname = 'TCD_Mod'
mod_dir = os.path.dirname(os.path.realpath(__file__))
fname = str(os.path.join(mod_dir, "TreeDensityTool.py"))

spec = importlib.util.spec_from_file_location(modname, fname)
if spec is None:
    raise ImportError(f"Could not load spec for module '{modname}' at: {fname}")
module = importlib.util.module_from_spec(spec)
sys.modules[modname] = module
try:
    spec.loader.exec_module(module)
except FileNotFoundError as e:
    raise ImportError(f"{e.strerror}: {fname}") from e


class TreeDensityToolClass(ToolDefinition):
    tool_name: str = "TreeDensityTool (Europe only)"
    metric_name: str = "Tree Cover Density"

    transproj = Transformer.from_crs(
        "WGS84",
        "EPSG:3035",
        always_xy=True,
    )

    @dataclass
    class TreeDensityToolArgs(ToolDefinitionArgs):
        radii: List[int] = field(default_factory=lambda: [100, 200, 500, 1000, 2000])
        disabled_as_zero: Optional[bool] = None

    @staticmethod
    @lru_cache
    def file_exists(file_name, country):
        for root, dirs, files in os.walk(os.path.dirname(os.path.realpath(__file__))):
            if file_name in files:
                return True

        logging.error(f"Couldn't find file {file_name}. "
                      f"The Tree Cover Density dataset is not downloaded for this coordinate. Please download it from "
                      f"https://land.copernicus.eu/en/products/high-resolution-layer-tree-cover-density/tree-cover-density-2018.\n"
                      f"Alternatively, is {country} not in Europe?")

        return False

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        transformed_coord = cls.transproj.transform(xx=lng, yy=lat)

        x = transformed_coord[0]
        y = transformed_coord[1]

        # find the closest pixel center (10m apart, 5 at the end)
        closest_x = int(round(x + 5, -1) - 5)
        closest_y = int(round(y + 5, -1) - 5)

        # The top left pixel of an image represents e.g. x = 4000005, y = 2999995
        # Y decrease as you go down on the image, X increases as you go right
        # We need to be careful as the top left pixel of an image is (0,0).
        # This means the geographical Y goes down as the y pixel goes up.
        # So now, we find the next lowest x value like this: XX00005, and the next *highest* y value like this: 29XXXX5.
        closest_x_below = closest_x + 5 - closest_x % 100000
        closest_y_above = closest_y - 5 - closest_y % 100000 + 100000

        # The files are split up. If our top left pixel is at XX00005, YY99995, then the file is called EXXNYY.
        x_file = closest_x_below // 100000
        y_file = closest_y_above // 100000

        file_name = f"TCD_2018_010m_E{x_file}N{y_file}_03035_v020.tif"

        country = reverse_geocode.get((lat, lng))["country"]

        return cls.file_exists(file_name, country)

    @classmethod
    def required_setup(cls):
        return

    @classmethod
    def main(cls, args: TreeDensityToolArgs) -> List[str]:
        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_dirs = []

        for disabled_as_zero in [True, False]:
            if args.disabled_as_zero is None or args.disabled_as_zero == disabled_as_zero:
                sysargs = [str(args.radii).replace(" ", ""), os.path.realpath(args.task_file_location)]
                if disabled_as_zero:
                    sysargs.append("disabled_as_zero")

                output_directories_before = []
                if os.path.isdir(os.path.join(tool_dir, "outputs")):
                    output_directories_before = [
                        os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
                    ]

                os.chdir(tool_dir)

                module.main(sysargs)

                os.chdir(correct_dir)

                output_directories_after = []
                if os.path.isdir(os.path.join(tool_dir, "outputs")):
                    output_directories_after = [
                        os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
                    ]

                new_directory = list(set(output_directories_after) - set(output_directories_before))
                assert len(new_directory) == 1, "Somehow, multiple output dirs were found."
                new_directory = new_directory[0]
                renamed = os.path.normpath(new_directory) + (
                        "_disabled_as_zero" if disabled_as_zero else "_disabled_discarded"
                )
                os.rename(new_directory, renamed)

                output_dirs.append(renamed)

        return output_dirs
