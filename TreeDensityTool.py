import ast
import os
import shutil
import sys
import re
from datetime import datetime

import geopandas as gpd
import hilbertcurve.hilbertcurve as hilbert

from pathlib import Path

from tqdm import trange
from shapely.geometry import Point
from typing import Tuple, List

from Mode100m import get_single_tree_density_100
from Mode10m import get_single_tree_density_10


def get_x_and_y_indices_from_task_file(infile: str) -> Tuple[int, int, bool]:
    x_index = 2
    y_index = 1
    sure = False

    with open(infile) as task_file:
        first_line = task_file.readline()
        first_line = re.split('[;,\\t]', first_line.strip())

        for i, value in enumerate(first_line):
            if value.lower() in {"y", "lat", "latitude"}:
                y_index = i
                sure = True
            elif value.lower() in {"x", "lng", "lon", "long", "longitude"}:
                x_index = i
                sure = True

    return x_index, y_index, sure


def get_frame_from_txt(infile: str) -> gpd.GeoDataFrame:
    x_index, y_index, sure_about_xy_indices = get_x_and_y_indices_from_task_file(infile)

    crs = "EPSG:4326"

    points = []
    ids = []

    with open(infile) as task_file:
        counter = 0
        for line in task_file.readlines():
            counter += 1
            line = line.strip()
            if not line:
                continue
            line_split = re.split('[;,\\t]', line)

            if len(line_split) <= x_index or not line_split[x_index]:
                continue
            if len(line_split) <= y_index or not line_split[y_index]:
                continue

            coordinate_id = line

            try:
                lat = float(line_split[y_index])
                lng = float(line_split[x_index])
            except Exception as e:
                if counter == 1:
                    continue
                print(f"Could not parse line:")
                print(line)
                print(f"Error message: {e}")
                continue

            if lat > 180:
                crs = "EPSG:25832"
                if not sure_about_xy_indices:
                    x_index, y_index = y_index, x_index
                    lat, lng = lng, lat

            point = Point(lng, lat)

            points.append(point)
            ids.append(coordinate_id)

    task_data = gpd.GeoDataFrame({"geometry": points}, index=ids, crs=crs)
    task_data = task_data.to_crs(epsg="3035")

    task_dict = task_data.to_dict()["geometry"]

    if len(task_dict) > 1:
        task_dict_arrays = {key: [value.x, value.y] for key, value in task_dict.items()}

        max_x = max(value[0] for value in task_dict_arrays.values())
        max_y = max(value[1] for value in task_dict_arrays.values())
        min_x = min(value[0] for value in task_dict_arrays.values())
        min_y = min(value[1] for value in task_dict_arrays.values())

        h_curve = hilbert.HilbertCurve(15, 2)

        max_distance = max(max_x - min_x, max_y - min_y)

        normalised_task_dict = {
            key: [(value[0] - min_x) * h_curve.max_x / max_distance, (value[1] - min_y) * h_curve.max_x/ max_distance]
            for key, value in task_dict_arrays.items()
        }

        hilbert_dict = {
            key: h_curve.distance_from_point(value) for key, value in normalised_task_dict.items()
        }

        task_dict = {k: v for k, v in sorted(task_dict.items(), key=lambda item: hilbert_dict[item[0]])}

    sorted_task_data = gpd.GeoDataFrame({"geometry": task_dict.values()}, index=task_dict.keys(), crs=crs)

    return sorted_task_data


def do_tree_density(infile: str, output_dir: str, radius=200, disabled_0=False, year=2018, resolution=10):
    print("---")
    print(f"Getting tree density for file {infile} with radius {radius}m for year {year}.")
    if disabled_0:
        print(f"\"Disabled as zero\" mode is on, meaning pixels with invalid values are treated as value 0.")
    else:
        print(f"\"Disabled as zero\" mode is off, meaning pixels with invalid values are discarded.")

    shutil.copyfile(infile, os.path.join(output_dir, os.path.basename(infile)))

    file_extension = Path(infile).suffix
    if file_extension in {".csv", ".txt"}:
        frame = get_frame_from_txt(infile)
    else:
        assert False, f"Don't know file extension {file_extension}"

    point_tuples = {geo_id: (point.y, point.x) for geo_id, point in frame.itertuples(index=True, name=None)}

    id_to_density = dict()

    items = list(point_tuples.items())

    if year in {2018} and resolution == 10:
        for i in trange(0, len(items)):
            id_to_density[items[i][0]] = get_single_tree_density_10(items[i][1][1], items[i][1][0], radius, disabled_0)
    elif year in {2015, 2018} and resolution == 100:
        for i in trange(0, len(items)):
            id_to_density[items[i][0]] = get_single_tree_density_100(
                items[i][1][1], items[i][1][0], radius, disabled_0, year
            )
    else:
        assert False, (f"Tree density dataset for year {year} and resolution {resolution}m"
                       f" does not exist or is not currently supported.")

    file_name = os.path.join(output_dir, f"output_{year}_resolution{resolution}_radius{radius}m" + ("_disabled_as_0" if disabled_0 else "") + ".csv")

    with open(file_name, "w") as output_file:
        x_index, y_index, _ = get_x_and_y_indices_from_task_file(infile)

        with open(infile) as task_file:
            counter = 0
            for line in task_file.readlines():
                counter += 1
                line = line.strip()
                if not line:
                    continue
                line_split = re.split('[;,\\t]', line)

                if len(line_split) <= x_index or not line_split[x_index]:
                    continue
                if len(line_split) <= y_index or not line_split[y_index]:
                    continue

                try:
                    _ = float(line_split[y_index])
                    _ = float(line_split[x_index])
                except Exception as e:
                    output_file.write(",".join(line_split) + ",amtOfUsedPixels,amtOfInvalidPixels,invalidPixelsTreatment,meanTCD,stddevTCD\n")
                    continue

                if counter == 1:
                    output_file.write("id,lat,lng,amtOfUsedPixels,amtOfInvalidPixels,invalidPixelsTreatment,meanTCD,stddevTCD\n")

                density = id_to_density[line]

                disabled_str = "Treated as 0" if disabled_0 else "Discarded"

                output_file.write(
                    ",".join(line_split) + f",{density[2]},{density[3]},{disabled_str},{density[0]},{density[1]}\n"
                )

    # print(max(((geo_id, density) for geo_id, density in id_to_density.items()), key=lambda x: x[1]))
    # print(min(((geo_id, density) for geo_id, density in id_to_density.items()), key=lambda x: x[1]))

    print(f"Done. View the output in {file_name}")


def output_readme(file_path: str, radii: List[int], years: List[int], resolution: int, disabled_as_0: bool):
    with open(file_path, "w") as f:
        f.write("TREE DENSITY TOOL\n\n"
                "This file contains some information on how the data was generated and how to interpret it.")

        f.write("\n\n-----\n\n"
                "TREE COVER DENSITY DATASET\n\n"
                "This tool uses the Copernicus Tree Cover Density dataset for Europe.\n"
                "This dataset consists of image files where each pixel represents the"
                " tree cover density in a square area.\n\n"
                "There are currently two supported versions of this dataset, the 10m version and the 100m version."
                f"\nIn this case, the {resolution}m version was used, meaning pixel corresponds to"
                f" a {resolution}m*{resolution}m area.\n\n"
                "Furthermore, this dataset is released 3-yearly.\n")

        year_text = f"In this case, the version of the dataset from {years[0]} was used."
        if len(radii) > 1:
            year_text = (f"In this case, the versions of the dataset "
                         f" {', '.join((str(year) for year in years[:-1]))}"
                         f" and {years[-1]} were used. For each year, you'll be able to find a different output file,"
                         " which will have the year in the title.")

        f.write(year_text + "\n\n"
                "The possible pixel values range from 0-100 for valid pixels, indicating a percentage."
                " There is also the value 255, which denotes an invalid pixel, usually because it is on water.")

        f.write("\n\n-----\n\n"
                "INPUT AND OUTPUT\n\n"
                "Each line in the task file defines a location.\n"
                "The Tree Density Tool will extract and average out the Tree Cover Density"
                " in a radius around each location.\n\n")

        radius_text = (f"In this case, the radius {radii[0]}m was used."
                       f" The output can be viewed in \"output_{radii[0]}.csv\".")
        if len(radii) > 1:
            radius_text = (f"In this case, the radii {', '.join((str(radius) for radius in radii[:-1]))}"
                           f" and {radii[-1]} were used. For each radius, you'll be able to find a"
                           " different output file, which will have the radius in the title.")

        f.write(radius_text + "\n")

        f.write("An output file usually contains the id, lat and lng of each location, followed by the Tree Cover"
                " Density values calculated by the tool.\n")
        f.write("The name of the output file will contain the year or the dataset, the resolution of the dataset,"
                " and the search radius used.\n"
                f"Example: output_{years[0]}_resolution{resolution}m_radius{radii[0]}m.csv")

        f.write("\n\n-----\n\n"
                "WHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

        if disabled_as_0 is None:
            f.write(f"Both values for the \"treat disabled pixels as 0\" parameter were used for this task.\n"
                    "You will find two sets of output files,"
                    " one set with \"disabled_as_zero\" at the end and one without.\n"
                    "For the files where \"disabled_as_zero\" is present, invalid pixels (value 255),"
                    " which usually refer to pixels on water, were just"
                    f" treated as pixels with the value 0, meaning they were included in the calculations.\n"
                    f"For the files where this suffix is not present, invalid pixels (value 255) were completely"
                    f" discarded.\n")
        elif disabled_as_0:
            f.write(f"The \"treat disabled pixels as 0\" parameter was set for this task.\n"
                    f"This means that invalid pixels (value 255), which usually refer to pixels on water, were just"
                    f" treated as pixels with the value 0, meaning they were included in the calculations.\n")
        else:
            f.write(f"The \"treat disabled pixels as 0\" parameter was NOT set for this task.\n"
                    f"This means that invalid pixels (value 255) were completely discarded.\n")

        f.write("The amount of used pixels and the amount of discarded pixels can be seen in the output parameters"
                " \"amtOfUsedPixels\" and \"amtOfInvalidPixels\" respectively in the output file.\n\n")

        f.write("After collecting all the pixels in a radius around a location, an arithmetic mean and standard"
                " deviation are calculated. These values are simple called \"meanTCD\" and \"stddevTCD\" in the"
                " output file.\n\n")

        f.write("Important note on edge pixels:\n")

        if resolution == 10:
            f.write(f"In the case of the dataset for {resolution}m*{resolution}m resolution,"
                    f" which was used for this task, it is simply determined"
                    " whether the majority of the pixel is inside the radius or not. If it is, it is counted fully.\n")

        elif resolution == 100:
            f.write(f"In the case of the dataset for {resolution}m*{resolution}m resolution,"
                    f" which was used for this task, the mean and standard "
                    f"deviation are weighted by how much of each pixel actually sits within the radius.\n"
                    f"Edge pixels are \"cut\" to determine how much of their area is within the circle.\n")


def main(args):
    default_task_file = "TwinLife_testdata_v1.0.0.csv"

    radii = [100, 200, 500, 1000, 2000]

    resolution = 100

    if resolution == 100:
        years = [2015, 2018]
    else:
        years = [2018]

    disabled_as_zero = None

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg == "disabled_as_zero":
            # Count pixels with 255 (out of range), which usually means they're in the water, as 0 (no trees).
            # Whereas normally, these pixels will just be discarded.
            disabled_as_zero = True

        elif arg.isnumeric():
            radii = [int(arg)]

        elif os.path.isfile(arg):
            default_task_file = arg

        else:
            print(f"Could not find file {arg}.")
            return -1

    if not os.path.isfile(default_task_file):
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        # print("For now, you need a taskfile in task.txt with each row being ID,lat,lng in WGS84 format.")
        # print("The ID has to just be some unique identifier.")
        return -1

    output_dir = os.path.join("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))
    os.makedirs(output_dir)

    if disabled_as_zero is None:
        for radius in radii:
            for year in years:
                do_tree_density(default_task_file, output_dir, radius, True, year, resolution)
                do_tree_density(default_task_file, output_dir, radius, False, year, resolution)
    else:
        for radius in radii:
            for year in years:
                do_tree_density(default_task_file, output_dir, radius, disabled_as_zero, year, resolution)

    output_readme(os.path.join(output_dir, "readme.txt"), radii, years, resolution, disabled_as_zero)


if __name__ == "__main__":
    main(sys.argv[1:])
