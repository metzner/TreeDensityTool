import math
import os
from functools import lru_cache
from typing import Union

import numpy as np
import shapely
import zarr
from shapely import geometry, Polygon
from tifffile import TiffFile


@lru_cache
def get_z_array(year=2018):
    if year == 2018:
        file_name = "TCD_2018_100m_eu_03035_V2_0.tif"
    else:
        file_name = f"TCD_{year}_100m_eu_03035_d04_full.tif"

    full_path = None

    for root, dirs, files in os.walk(os.path.dirname(os.path.realpath(__file__))):
        if file_name in files:
            full_path = os.path.join(root, file_name)

    if not full_path:
        print(f"Couldn't find {file_name}. Please download the Copernicus 2018 tree density dataset.")
        assert False

    #  print(f"Retrieving {full_path}.")

    assert os.path.isfile(full_path), f"Could not find file {file_name}"

    tif = TiffFile(full_path)
    store = tif.aszarr()
    return zarr.open(store, mode="r")


def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- NumPy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise:
    variance = np.average((values - average) ** 2, weights=weights)
    return average, math.sqrt(variance)


def get_single_tree_density_100(xx: Union[int, float], yy: Union[int, float], radius: Union[int, float], disabled_0: bool, year: int):
    if xx == math.nan or yy == math.nan:
        return "None", "None", "None"

    z_array = get_z_array(year)
    y_size, x_size = z_array.shape

    pixel_width = 100
    top_left_x_center = 900050.0000000000
    top_left_y_center = 5499950.0000000000
    bottom_right_x_center = top_left_x_center + pixel_width * (x_size - 1)
    bottom_right_y_center = top_left_y_center - pixel_width * (y_size - 1)

    assert top_left_x_center < xx < bottom_right_x_center, top_left_y_center > yy > bottom_right_y_center

    radius_poly = geometry.Point(xx, yy).buffer(radius, resolution=40)
    buffer_poly = geometry.Point(xx, yy).buffer(radius + 200, resolution=40)

    min_x, min_y, max_x, max_y = buffer_poly.bounds
    min_x_pixel, max_x_pixel, min_y_pixel, max_y_pixel = (
        round((min_x - top_left_x_center) / pixel_width), round((max_x - top_left_x_center) / pixel_width),
        round((top_left_y_center - max_y) / pixel_width), round((top_left_y_center - min_y) / pixel_width),
    )

    areas_and_values = []

    for x in range(min_x_pixel, max_x_pixel + 1):
        for y in range(min_y_pixel, max_y_pixel + 1):
            if x > x_size or x < 0 or y > y_size or y < 0:
                raise ValueError("x and y went over the borders of an image. There is no handling for this yet")

            pixel_left_top = (top_left_x_center + (x - 0.5) * pixel_width, top_left_y_center - (y - 0.5) * pixel_width)
            pixel_right_bottom = (pixel_left_top[0] + pixel_width, pixel_left_top[1] - pixel_width)
            pixel_right_top = (pixel_right_bottom[0], pixel_left_top[1])
            pixel_left_bottom = (pixel_left_top[0], pixel_right_bottom[1])

            poly = Polygon([pixel_left_top, pixel_right_top, pixel_right_bottom, pixel_left_bottom])
            poly: Polygon = shapely.intersection(poly, radius_poly)
            if not shapely.is_empty(poly):
                pixel_value = z_array[(y, x)]
                areas_and_values.append((poly.area, pixel_value))

    if not areas_and_values:
        raise ValueError(f"Couldn't get any pixels for {xx},{yy}")

    len_before = len(areas_and_values)

    unknown_values = [value for _, value in areas_and_values if not (0 <= value <= 100) and not value == 255]

    if unknown_values:
        print(f"Encountered unknown pixel values: {set(unknown_values)}.")

    if disabled_0:
        areas_and_values = [(area, value) if (1 <= value <= 100) else (area, 0) for area, value in areas_and_values]
        len_after = sum((0 <= value <= 100) for _, value in areas_and_values)
    else:
        areas_and_values = [(area, value) for area, value in areas_and_values if (0 <= value <= 100)]
        len_after = len(areas_and_values)

    if not len_after:
        print(f"None of the pixels for {xx},{yy} were valid.")
        return ".",".",len_after, len_before - len_after

    elif len_after == 1:
        print(f"Only one of of the pixels for {xx},{yy} was valid, which means stddev cannot be calculated.")
        avg, stddev = weighted_avg_and_std([v for _, v in areas_and_values], [a for a, _ in areas_and_values])
        return round(avg, 5), ".", len_after, len_before - len_after

    elif len_before != len_after:
        print(f"Some values were invalid for {xx},{yy}. Out of {len_before} pixels, {len_after} were valid.")

    avg, stddev = weighted_avg_and_std([v for _, v in areas_and_values], [a for a, _ in areas_and_values])
    return round(avg, 5), round(stddev, 5), len(areas_and_values), len_before - len_after
